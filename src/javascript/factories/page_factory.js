angular.module("app.PageFactory", [])
  .factory("Page", ["$resource", function($resource) {
    return $resource("http://localhost:3001/api/v1/pages/:id", null, {
      update: {
        method: 'PUT'
      }
    });
  }]);