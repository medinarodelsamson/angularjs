angular.module("app.router", [])
  .config(['$stateProvider', '$urlRouterProvider', '$locationProvider', function($stateProvider, $urlRouterProvider, $locationProvider) {

    // $locationProvider.html5Mode(true);
    $urlRouterProvider.otherwise("/");

    $stateProvider
      .state('home', {
        url: "/",
        templateUrl: "/javascript/templates/home.html"
      })
      .state('about', {
        url: "/about",
        templateUrl: "/javascript/templates/about.html"
      })
      .state('contact', {
        url: "/contact",
        templateUrl: "/javascript/templates/contact.html"
      })
      .state('home.pageEdit', {
        url: "^/pages/:id",
        views: {
          pageContent: {
            templateUrl: "/javascript/templates/pages/edit.html"
          }
        }
      });
  }]);