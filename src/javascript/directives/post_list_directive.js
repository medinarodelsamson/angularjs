/*
  <page-list></page-list>
*/
angular.module("app.PageListDirective", ['ui.tinymce'])
  .directive("pageList", function() {
    linker = function(scope, element, attrs) {
      $(element).on('click', function(){
        console.log(this);
      })
    };
    return {
      restrict: 'E',
      templateUrl: "/javascript/templates/pages/li.html",
      link: linker

    };
  })
  .directive("scopedForm", function() {
    // linker = function(scope, element, attrs) {
    //   tinymce.init({
    //     selector: $(element).id
    //   });
    // }
    return {
      restrict: 'E',
      scope: {
        page: "=",
        tinymceOptions: "="
      },
      templateUrl: "/javascript/templates/pages/page_form.html",
      // link: linker

    };
  });


/*
  <textarea class="tinyMCE"></textarea>
*/
// angular.module("app.TinyMCEDirective", [])
//   .directive("tinyMCE", function() {
//     linker = function(scope, element, attrs) {
//       $(element).tinyMCE();
//     }

//     return {
//       restrict: 'C',
//       link: linker
//     };
//   });