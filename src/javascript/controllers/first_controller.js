angular.module("app.FirstController", [])
    .controller("FirstController", ['$scope', '$http', 'Page', 'SampleService', function ($scope, $http, Page, SampleService) {
     $scope.pages = [];
     $scope.addPage = {};

     $scope.shout = function() {
      console.log('jebs');
     };

     $scope.addPost = function(){
      Page.save($scope.addPage, function(resp){
        SampleService.pages.push(resp);
      });
     };

     $scope.deletePost = function(page) {
       Page.delete({id: page.id},function(resp){
        var index = $scope.pages.indexOf(page);
        SampleService.pages.splice(index, 1);
       });
     };

     function getAll() {
       Page.query(function(resp){
          SampleService.pages = resp;
          $scope.pages = SampleService.pages;
       });
     }

     getAll();


  }]);