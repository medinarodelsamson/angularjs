  angular.module("app.PageDetailController", [])
  .controller("PageDetailController", ['$scope', 'Page', '$stateParams', 'SampleService', function ($scope, Page, $stateParams, SampleService) {
     $scope.currentPage = {};
     Page.get({id: $stateParams.id},function(resp){
        $scope.currentPage = resp;
     });

     $scope.savePost = function() {
       Page.update({id: $scope.currentPage.id}, $scope.currentPage, function(resp){
        var index = SampleService.pages.indexOf(SampleService.pages.filter(function(t) {
          return t.id === $scope.currentPage.id;
        })[0]);

        console.log(SampleService.pages[index] = resp);
       });
     };

  $scope.tinymceOptions = {
    plugins: 'link image code',
    toolbar: 'undo redo | bold italic | alignleft aligncenter alignright | code'
  };
  }]);