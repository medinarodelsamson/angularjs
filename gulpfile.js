var gulp = require('gulp'),
    sass = require('gulp-sass'),
    uglify = require('gulp-uglify'),
    bower = require('gulp-bower'),
    concat = require('gulp-concat'),
    clean = require('gulp-clean'),
    jshint = require('gulp-jshint'),
    jade = require('gulp-jade'),
    gutil = require('gulp-util'),
    runSequence = require('gulp-run-sequence');
    browserSync = require("browser-sync").create(),
    reload = browserSync.reload
    

/*
 * run: gulp serve
 */
gulp.task('serve', ['default'], function() {
    browserSync.init({
        server: "./public"
    });
});

var path = {
    scss: 'src/scss/**/*.scss',
    js: 'src/javascript/**/*.js',
    vendor: 'src/vendor/**/*.js',
    images: 'src/image/**/*',
    html: [
        'src/*.jade',
        'src/templates/**/*.jade'
    ],
    templates: 'src/javascript/**/*.jade',
    fonts: 'src/fonts/**/*',
    bower_components: './bower_components'
}

// delete prev public, compile and watch
gulp.task('default', [
    'build',
    'watch'
]);

// watch
gulp.task('watch', function() {
    gulp.watch(path.scss, [
        'build_css'
    ]);
    gulp.watch(path.js, [
        'build_js', 'jshint'
    ]);
    gulp.watch(path.templates, [
        'build_js_templates'
    ]);
    gulp.watch(path.vendor, [
        'copy_vendor'
    ]);
    gulp.watch(path.images, [
        'copy_images'
    ]);
    gulp.watch(path.html, [
        'build_html'
    ]);
    gulp.watch(path.fonts, [
        'copy_fonts'
    ]);
    gulp.watch(path.bower_components, [
        'copy_bower_components'
    ]);
});

/*
 * task: clean
 * $ gulp clean
 */
gulp.task('clean', function() {
    return gulp.src('./public', {
        read: false,
        force: true
    }).pipe(clean());
});

/*
 * task: jshint
 * $ gulp jshint
 */
gulp.task('jshint', function() {
    return gulp.src(path.js)
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});
/*
 * task: build_css
 * $ gulp build_css
 */
gulp.task('build_css', function() {
    return gulp.src(path.scss)
        .pipe(sass())
        .pipe(gulp.dest('public/stylesheets'))
        .pipe(reload({
            stream: true
        }));
});

/*
 * task: build_js
 * $ gulp build_js
 */
gulp.task('build_js', function() {
    return gulp.src([
            'src/javascript/factories/**/*.js',
            'src/javascript/controllers/**/*.js',
            path.js
        ])
        .pipe(concat('app.js'))
        // .pipe(uglify().on('error', gutil.log))
        .pipe(gulp.dest('public/javascript'))
        .pipe(reload({
            stream: true
        }));
});

/*
 * task: copy_vendor
 * $ gulp copy_vendor
 */
gulp.task('build_js_templates', function() {
    return gulp.src(path.templates)
        .pipe(jade())
        .pipe(gulp.dest('public/javascript'))
        .pipe(reload({
            stream: true
        }));
});

/*
 * task: copy_vendor
 * $ gulp copy_vendor
 */
gulp.task('copy_vendor', function() {
    return gulp.src(path.vendor)
        .pipe(gulp.dest('public/vendor'))
        .pipe(reload({
            stream: true
        }));
});

/*
 * task: copy_images
 * $ gulp copy_images
 */
gulp.task('copy_images', function() {
    return gulp.src(path.images)
        .pipe(gulp.dest('public/image'))
        .pipe(reload({
            stream: true
        }));
});

/*
 * task: build_html
 * $ gulp build_html
 */
gulp.task('build_html', function() {
    return gulp.src(path.html)
        .pipe(jade())
        .pipe(gulp.dest('public'))
        .pipe(reload({
            stream: true
        }));
})

/*
 * task: copy_bower_components
 * $ gulp copy_bower_components
 */
gulp.task('copy_bower_components', function() {
    return bower()
        .pipe(gulp.dest('public/bower_components'))
        .pipe(reload({
            stream: true
        }));
});

/*
 * task: copy_fonts
 * $ gulp copy_fonts
 */
gulp.task('copy_fonts', function() {
    return gulp.src(path.fonts)
        .pipe(gulp.dest('public/fonts'))
        .pipe(reload({
            stream: true
        }));
});

/*
 * task: build
 * $ gulp build
 */
gulp.task('build', function(cb){
    runSequence('clean',
        'build_html',
        'build_js',
        'build_js_templates',
        'build_css',
        'copy_bower_components',
        'copy_images',
        'copy_fonts',
        'copy_vendor',
        cb
    );
});