# Gulp Starter
I'm learning how to use Gulp and Gulp stacks from scratch

## Setup
- ```git clone git@bitbucket.org:medinarodelsamson/angularjs.git```
- ```npm install```
- ```bower install```

## Start
- ```gulp serve```

## Build
- ```gulp build```

You should see the compiled files under ./public folder


## Note
You should have npm installed:
- ```npm install --global gulp```
